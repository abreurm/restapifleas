'use stric' //para poder usar nuevas funciones en javascript
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var publicacion_schema = ({
    titulo:{
        type: String,
        minlength: [2, "Minimo 2 caracteres"],
        maxlenght: [20, "Maximo 20 caracteres"],
        required: "El titulo  es obligatorio"
    },
    precio: {
        type: Number,
        minlength: [2, "Minimo 2 digitos"],
        maxlenght: [15, "Maximo 15 digitos"],
        required: "El  precio es obligatorio"

    },
    descripcion:{
        type: String,
        minlength: [10, "Minimo 10 caracteres"],
        maxlenght: [500, "Maximo 500 caracteres"],
        required: "La descripcion  es obligatorio"
    },
   categoria:{
       type: Schema.Types.ObjectId,
       ref: "Categoria"
    },
    estado :{
        type: Schema.Types.ObjectId,
        ref: "Estado"

    }, 
    direccion:{
        type: String,
        minlength: [10, "Minimo 10 caracteres"],
        maxlenght: [50, "Maximo 50 caracteres"],
        required: "La direccion es obligatoria"

    },
    fecha:{
        type: Date,
        default: Date.now
    },
    creador: {
        type: Schema.ObjectId,
        ref: "Usuario",
        required: "Id es obligatoria"
    }
 
});
module.exports = mongoose.model("Publicacion", publicacion_schema);