'use stric' //para poder usar nuevas funciones en javascript
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var categoria_schema= ({
    nombre: {
        type: String,
        minlength: [2, "Minimo 2 caracteres"],
        maxlength: [50, "Maximo 50 caracteres"],
        required: "El nombre  de la categoria es obligatorio",
    },
    icono:{
        type:String,
        minlength: [2, "Minimo 2 caracteres"],
        maxlength: [50, "Maximo 50 caracteres"],
        required: "El icono es obligatorio",
    }

});
module.exports = mongoose.model("Categoria", categoria_schema);