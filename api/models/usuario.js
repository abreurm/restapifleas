'use stric' //para poder usar nuevas funciones en javascript
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var sexo=["F","M"];

var usuario_schema= new Schema({
    nombre:{ 
        type: String, 
        minlength: [2, "Minimo 2 caracteres"], 
        maxlenght: [40,"Maximo 40 caracteres"],
        required: "El nombre es obligatorio"
    },
    apellido:{
        type:String,
        minlength:[2,"Minimo 2 caracteres"],
        maxlenght:[40," Maximo 40 caracteres"],
        required: "El apellido es obligatorio"
    },
    email:{ 
        type: String, 
        required: "El email es obligatorio" 
    },
    password:{ 
        type: String, 
        minlength: [6, "Minimo 6 caracteres"],
        maxlength: [18, "Maximo 18 caracteres"],
        required: "La clave es obligatoira" 
    },
   estado:{
        type: Schema.ObjectId, 
        ref: "Estado"
    },   
    direccion:{
        type:String,
        minlength: [10,"Minimo 10 caracteres"],
        maxlenght: [300,"Maximo 300 caracteres"],
        required: "La direccion es obligatoria"
    },
    celular:{
        type: String,
        minlength: [11, "Minimo 11 caracteres"],
        maxlenght: [11, "Maximo 11 caracteres"],
        required: "El  celular  es obligatoria"
    }
});

module.exports= mongoose.model("Usuario", usuario_schema);