'use stric' //para poder usar nuevas funciones en javascript
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var estado_schema= new Schema({
    nombre:{
        type: String,
        minlength: [4, "Minimo 6 caracteres"],
        maxlength: [20, "Maximo 20 caracteres"],
        required: "El nombre es obligatorio", 
    }
});

module.exports= mongoose.model("Estado", estado_schema);