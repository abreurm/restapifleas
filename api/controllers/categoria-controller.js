var Categoria= require('../models/categoria');

module.exports = {
    getCategoria: function (req, res) {
        Categoria.find({}, function (err, categorias) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ categorias: categorias });
            }
        });
    },

    getCategoriaId: function (req, res) {
        Categoria.findById(req.params.id, function (err, categoria) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ categoria: categoria });
            }
        });
        
    },
    save: function (req, res) {
        var categoria = new Categoria({
            nombre: req.body.nombre,
            icono: req.body.icono

        });
        categoria.save(function (err, categoria) {
            /* If we encounter an error send the details as a HTTP response */
            if (err) {
                res.status(500).send(err)
            } else {
                /* If all is good then send a JSON encoded map of the retrieved data
              as a HTTP response */
                res.json({ categoria: categoria });
            }
        });

    },
    update: function (req, res) {
        CanvasRenderingContext2D.findById(req.params.id, function (err, categoria) {
            if (err) {
                console.dir(err)
            } else {
                categoria.nombre = req.body.nombre;
                categoria.icono = req.body.icono;
                
                 categoria.save(function (err, categoria) {
                    if (err) {
                        res.status(500).send(err)
                    } else {
                        res.json({ categoria: categoria });
                    }
                });
            }
        })
    },
    delete: function (req, res) {
        Categoria.findByIdAndRemove({ _id: req.params.id }, function (err, categoria) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ categoria: categoria });
            }
        });
    }

}    