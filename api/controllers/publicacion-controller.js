var Usuario = require('../models/usuario');
var Estado = require('../models/estado');
var Publicacion = require('../models/publicacion');
var Categoria = require('../models/categoria');

module.exports = {

    getPublicacion: function (req, res) {
        Publicacion.find({}, function (err, publicaciones) {
            Usuario.populate(publicaciones, { path: "creador" }, function (err, publicaciones){
                Estado.populate(publicaciones,{ path: "estado"},function(err,publicacion){
                    if (err) {
                        console.dir(err);
                    } else {
                        res.json({ publicaciones: publicaciones });
                        console.log("sesion de:" + res.locals.user_id)
                    }
                })
            })
        });
    },

    getPublicacionCategoria: function (req, res) {
        Publicacion.find({ categoria: req.params.id }, function (err, publicaciones) {
            Estado.populate(publicaciones, { path: "estado" }, function (err, publicacion) {
                if (err) {
                    console.dir(err);
                } else {
                    res.json({ publicacion: publicacion });
                }
            })
        });
    },

    getPublicacionUsuario: function (req, res) {
        Publicacion.find({ creador: req.params.id }, function (err, publicaciones) {
            Categoria.populate(publicaciones, { path: "categoria" }, function (err, publicaciones) {
                Estado.populate(publicaciones, { path: "estado" }, function (err, publicacion) {
                    if (err) {
                        console.dir(err);
                    } else {
                        res.json({ publicaciones: publicaciones });
                    }
                })
            })
        });
    },

    getPublicacionId: function(req,res){
        Publicacion.findById(req.params.id, function (err, publicacion) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ publicacion: publicacion });
            }
        });
    },

    getPublicacionTitulo: function(req,res){
        var ExpReg = new RegExp(req.params.titulo);
        Publicacion.find({ titulo: /fgfg/}, function(err, publicacion){
            if (err) {
                console.dir(err);
            } else {
                res.json({ publicacion: publicacion });
            }
        });
    },

    save: function (req, res) {
        var publicacion = new Publicacion({
            titulo: req.body.titulo,
            precio: req.body.precio,
            descripcion: req.body.descripcion,
            categoria: req.body.categoria,
            direccion: req.body.direccion,
            estado:req.body.estado,
            creador:req.params.iduser
        });
        console.log("Publicado por:"+req.params.iduser)
        publicacion.save(function (err, publicacion) 
        {
            /* If we encounter an error send the details as a HTTP response */
            if (err) {
                res.status(500).send(err)
            } else {
                /* If all is good then send a JSON encoded map of the retrieved data
              as a HTTP response */
                res.json({ publicacion: publicacion });
            }
        });
    },

    update: function (req, res) {
        Publicacion.findById(req.params.id, function (err, publicacion) {
            if (err) {
                console.dir(err)
            } else {
                publicacion.titulo = req.body.titulo;
                publicacion.precio = req.body.precio;
                publicacion.descripcion = req.body.descripcion;
                publicacion.direccion = req.body.direccion;
                
                publicacion.save(function (err, publicacion) {
                if (err) {
                    res.status(500).send(err)
                } else {
                    res.json({ publicacion: publicacion });
                }
                });
            }
        })
    },
    delete: function (req, res) {
        Publicacion.findByIdAndRemove({ _id: req.params.id }, function (err, publicacion) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ publicacion: publicacion });
            }
        });
    }

}