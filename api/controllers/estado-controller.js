var Estado = require('../models/estado');

module.exports={
    getEstados: function(req,res){
        Estado.find({},function(err, estados){
            if (err) {
                console.dir(err);
            } else {
                res.json({ estados: estados });
            }
        })
    },

    getEstadoId: function(req,res){
        Estado.findById(req.params.id, function (err, estado) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ estado: estado });
            }
        });
    },

    save: function(req, res){
        var estado= new Estado({
            nombre: req.body.nombre
        });
        estado.save(function(err, estado){
            if (err) {
                res.status(500).send(err)
            } else {
                /* If all is good then send a JSON encoded map of the retrieved data
              as a HTTP response */
                res.json({ estado: estado });
            }  
        });
    },

    update: function(req, res){
        Estado.findById(req.params.id, function(err,estado){
            console.log("Esto es lo que me llego: "+req.params.id);
            if(err){
                console.dir(err);
            }else{
                estado.nombre = req.body.nombre || estado.nombre;
                estado.save(function(err,estadoM){
                    if (err) {
                        res.status(500).send(err)
                    } else {
                        res.json({ estado: estadoM });
                    }
                })
            }
        });
    },

    delete: function(req, res){
        Estado.findByIdAndRemove(req.params.id , function (err, estado) {
            if (err) {
                console.dir(err);
            } else {
                res.json({ estado: estado });
            }
        });
    }
}