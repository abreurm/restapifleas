var Usuario = require('../models/usuario');
var Estado = require('../models/estado');

module.exports={

    getUsuarios: function(req,res){
        Usuario.find({},function(err,usuarios){
            if(err){
                console.dir(err);
            }else{
                res.json({ usuarios: usuarios });
            }
        });
    },

    getCorreo: function(req,res){
        Usuario.findOne({ 'email': req.params.email}, function(err, usuarios){
            Estado.populate(usuarios,{path:"estado"}, function(err, usuario){
                if (err) {
                    console.dir(err);
                } else {
                    if (usuario == null) {
                        res.json('no encontrado');
                    } else {
                        res.json({ usuario: usuario });
                    }
                }
            })
        })
    },

   
    getUsuarioId: function (req, res) {
        Usuario.findById(req.params.id, function (err, usuarios) {
            Estado.populate(usuarios, { path: "estado" }, function (err, usuario){
                if (err) {
                    console.dir(err);
                } else {
                    res.json({ usuario: usuario });
                }
            })      
        });
    },

    save: function(req,res){
        var usuario= new Usuario({
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            password: req.body.password,
            estado: req.body.estado,
            direccion: req.body.direccion,
            celular: req.body.celular
        });
        usuario.save(function(err,usuario){
            /* If we encounter an error send the details as a HTTP response */
            if (err) {
                res.status(500).send(err)
            } else {
                /* If all is good then send a JSON encoded map of the retrieved data
              as a HTTP response */
                res.json({ usuario: usuario });
            }     
        });
    },

    update: function(req,res){
        Usuario.findById(req.params.id, function (err, usuario) {
            if(err){
                console.dir(err)
            }else{
                usuario.nombre = req.body.nombre,
                usuario.apellido = req.body.apellido,
                usuario.email = req.body.email,
                usuario.password = req.body.clave,
                usuario.estado = Estado,
                usuario.direccion = req.body.direccion,
                usuario.celular = req.body.celular

                usuario.save(function (err,usuario){
                    if(err){
                        res.status(500).send(err)
                    }else{
                        res.json({ usuario: usuario });
                    }
                });
            }
        })
    },

    delete: function(req,res){
        Usuario.findByIdAndRemove({ _id: req.params.id }, function(err, usuario) {
            if (err) {
                console.dir(err);
            }else{
                res.json({ usuario: usuario });
            }
        });
    }
}