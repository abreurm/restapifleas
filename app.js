var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var config = require('./config');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
//login
//var session = require('express-session');
/*var cookieSession= require('cookie-session');
var session_middlewares = require("./api/middlewares/session");*/

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
/* Manage size limits for POST/PUT requests */
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

app.use(express.static(path.join(__dirname, 'public')));
//login
/*app.use(cookieSession({
  name: "session",
  keys: ["llave-1","llave-2"]
}));*/

/*Administrar acceso CORS para TODAS las requests/responses */
app.use(function (req, res, next) {
  /* Allow access from any requesting client */
  res.setHeader('Access-Control-Allow-Origin', '*');

  /* Allow access for any of the following Http request types */
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');

  /* Set the Http request header */
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
  next();
});

// conexion a Mongo
connection = mongoose.connect(config.database),

//app.use('/api', session_middlewares);
app.use('/api', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
