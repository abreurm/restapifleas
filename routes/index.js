var express = require('express');

var estadoCtrl=require("../api/controllers/estado-controller");
var publicacionCtrl=require("../api/controllers/publicacion-controller");
var categoriaCtrl=require("../api/controllers/categoria-controller");
var usuarioCtrl=require("../api/controllers/usuario-controller");

module.exports = (function () {
    
	var api = express.Router();
  
    api.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
    console.log("Session de:"+req.session.user_id);
    });

    /*PETICIONES PARA EL ESTADO*/
    api.route('/estados')
        .get(estadoCtrl.getEstados) 
        .post(estadoCtrl.save);

    api.route('/estados/:id')
        .get(estadoCtrl.getEstadoId)
        .put(estadoCtrl.update)
        .delete(estadoCtrl.delete);

    /*PETICIONES PARA LA PUBLICACION*/
    api.route('/publicaciones') // obtener todas las publicaciones
        .get(publicacionCtrl.getPublicacion)
        
    api.route('/publicaciones/usuario/:id') // obtener publicaciones por usuario
        .get(publicacionCtrl.getPublicacionUsuario)

    api.route('/publicar/:iduser') //guardar pubicacion con id del creador
        .post(publicacionCtrl.save);

    api.route('/publicaciones/:id') 
        .get(publicacionCtrl.getPublicacionId) 
        .put(publicacionCtrl.update)
        .delete(publicacionCtrl.delete); 
    
    api.route('/busqueda/titulo/:titulo')
        .get(publicacionCtrl.getPublicacionTitulo);   
    
    api.route('/busqueda/categoria/:id')
        .get(publicacionCtrl.getPublicacionCategoria);    
        
    /*PETICIONES CATEGORIA */
    api.route('/categorias')
        .get(categoriaCtrl.getCategoria)
        .post(categoriaCtrl.save);

    api.route('/categorias/:id')
        .get(categoriaCtrl.getCategoriaId)
        .put(categoriaCtrl.update)
        .delete(categoriaCtrl.delete);    

        /*PETTICIONES PARA EL USUARIO */
    api.route('/usuario')
        .post(usuarioCtrl.save)
        .put(usuarioCtrl.update)
        .delete(usuarioCtrl.delete);

  /*  api.route('/login')
        .post(usuarioCtrl.getLogin);*/

    api.route('/usuario/:id')
        .get(usuarioCtrl.getUsuarioId);

    api.route('/usuario/email/:email')
        .get(usuarioCtrl.getCorreo)    
    return api;
})();
